var interval;
var length = 0;
var filename = "Error_Log" + " " +Time_get() + ".txt";

var numb_error = new Array(30);
for (let q = 0; q < numb_error.length; q++) {
    numb_error[q] = 0;
}
var numb_stop = new  Array(30);
for (let p = 0; p < numb_stop.length; p++) {
    numb_stop[p] = 0;
}

function description() {
    var stat = new XMLHttpRequest();
    var commp = [];
stat.onload = function() {
  if(stat.readyState === 4) {
  var commpoints = JSON.parse(stat.responseText);
    var s = '';
    length = commpoints.Workflows.length;
    for(var i = 0; i < commpoints.Workflows.length; i++){
      for(var j = 0; j < commpoints.Workflows[i].Routes.length; j++){
        commp[j] = commpoints.Workflows[i].CommunicationPoints[j].API;
        if(j === 0){
          callcommpoints_1(commp[j]);
        }
        if(j === 1){
          callcommpoints_2(commp[j]);
        } 
    }
  }
}
    interval = setTimeout(description, 30000);
  }
stat.open('GET', 'Json/JSON Example.json');
stat.send();
}
description();

function callcommpoints_1(commp) {
  var xhttp_one = new XMLHttpRequest();
  
  xhttp_one.onreadystatechange = function() {
    if(xhttp_one.readyState === 4 ) {
      var commpoints = JSON.parse(xhttp_one.responseText);
      console.log(commpoints);
      
      var statusHTML = '';
      var error = '';
      var retrying = '';
      var running = '';
        statusHTML += '<tr>';
        if(commpoints.data.state === 'RUNNING') {
          statusHTML += '<tr class = "success">';
          statusHTML += '<td>'+ commpoints.data.id + '</td>';
          statusHTML += '<td>'+ commpoints.data.name + '</td>';
          statusHTML += '<td>'+ commpoints.data.outQueueSize + '</td>';
          statusHTML += '<td>' + commpoints.data.receivedCount + '</td>';
          statusHTML += '<td>' + commpoints.data.sentCount + '</td>';
          statusHTML += '<td>' + commpoints.data.state + '</td>';

          statusHTML += '<td>' +
          '<button class="btnnn btn-warning">RESTART</button>'
          + '</td>';

          running = '<button type = "button"  class="btn btn-success ">' + commpoints.data.state + '</button>';
          console.log(statusHTML);
        } else if(commpoints.data.state === 'RETRYING') {
          statusHTML += '<tr class = "warning">';
          statusHTML += '<td>'+ commpoints.data.id + '</td>';
          statusHTML += '<td>'+ commpoints.data.name + '</td>';
          statusHTML += '<td>'+ commpoints.data.outQueueSize + '</td>';
          statusHTML += '<td>' + commpoints.data.receivedCount + '</td>';
          statusHTML += '<td>' + commpoints.data.sentCount + '</td>';
          statusHTML += '<td>' + commpoints.data.state + '</td>';

          statusHTML += '<td>' +
          '<button class="btnnn btn-warning">RESTART</button>'
          + '</td>';
          
          retrying = '<button type = "button"  class="butnn btn-warning ">' + commpoints.data.state + '</button>';
        } else if(commpoints.data.state === 'STOPPED') {
          statusHTML += '<tr class = "danger">';
          statusHTML += '<td>'+ commpoints.data.id + '</td>';
          statusHTML += '<td>'+ commpoints.data.name + '</td>';
          statusHTML += '<td>'+ commpoints.data.outQueueSize + '</td>';
          statusHTML += '<td>' + commpoints.data.receivedCount + '</td>';
          statusHTML += '<td>' + commpoints.data.sentCount + '</td>';
          statusHTML += '<td>' + commpoints.data.state + '</td>';

          statusHTML += '<td>' +
          '<button class="btnnn btn-warning">RESTART</button>'
          + '</td>';

          
          numb_stop[0] = 1;
          stopped = '<button type = "button"  class="butnn btn-danger ">' + Count_stopped() + '</button>';
        /*commpoints_stopped(commpoints.data.state, commpoints.data.name);*/

        }else if(commpoints.data.state === 'ERROR'){
          statusHTML += '<tr class = "danger">';
          statusHTML += '<td>'+ commpoints.data.id + '</td>';
          statusHTML += '<td>'+ commpoints.data.name + '</td>';
          statusHTML += '<td>'+ commpoints.data.outQueueSize + '</td>';
          statusHTML += '<td>' + commpoints.data.receivedCount + '</td>';
          statusHTML += '<td>' + commpoints.data.sentCount + '</td>';
          statusHTML += '<td>' + commpoints.data.state + '</td>';

          statusHTML += '<td>' +
          '<button class="btnnn btn-warning">RESTART</button>'
          + '</td>';

          numb_error[0] = 1;
          error = '<button type = "button"  class="butnn btn-danger ">' + Count_error() + '</button>';
         /* commpoints_error(commpoints.data.state, commpoints.data.name );*/
        }
        statusHTML += '</tr>';
  
        document.querySelector('.commstatus_1 tbody.comm_1').innerHTML = statusHTML;
        document.querySelector('.commstatus_2 tbody.comm_1').innerHTML = statusHTML;
        document.querySelector('.commstatus_3 tbody.comm_1').innerHTML = statusHTML;
        document.querySelector('.commstatus_4 tbody.comm_1').innerHTML = statusHTML;
        if(error !== null && error !== undefined && (error !== ""))
          {
            document.querySelector('#target ').innerHTML = error;
            document.querySelector('#target_1 ').innerHTML = error;
            document.querySelector('#target_2 ').innerHTML = error;
            document.querySelector('#target_3 ').innerHTML = error;
          }else if(retrying !== null && retrying !== undefined && (retrying !== ""))
          {
          document.querySelector('#target').innerHTML = retrying;
          document.querySelector('#target_1 ').innerHTML = retrying;
          document.querySelector('#target_2').innerHTML = retrying;
          document.querySelector('#target_3 ').innerHTML = retrying;
          }else if(stopped !== null && stopped !== undefined && (stopped !== ""))
          {
          document.querySelector('#target').innerHTML = stopped;
          document.querySelector('#target_1 ').innerHTML = stopped;
          document.querySelector('#target_2').innerHTML = stopped;
          document.querySelector('#target_3 ').innerHTML = stopped;
          }else if(running !== null && running !== undefined && (running !== "")){
            document.querySelector('#target ').innerHTML = running;
            document.querySelector('#target_1 ').innerHTML = running;
            document.querySelector('#target_2 ').innerHTML = running;
            document.querySelector('#target_3 ').innerHTML = running;
          }else {
            var info = '';
            info = '<button type = "button"  class="butnn btn-danger " valid = "NO ERROR">'  + '</button>';
          }

        interval = setTimeout(callcommpoints_1, 30000);
    }
  }
  
      xhttp_one.open('GET', commp, true);
      xhttp_one.send();
  
  }
  callcommpoints_1(); 


  function callcommpoints_2(commp) {
    var xhttp_one = new XMLHttpRequest();
    
    xhttp_one.onreadystatechange = function() {
      if(xhttp_one.readyState === 4 ) {
        var commpoints = JSON.parse(xhttp_one.responseText);
        console.log(commpoints);
        
        var statusHTML = '';
        var error = '';
        var retrying = '';
        var running = '';
          statusHTML += '<tr>';
          if(commpoints.data.state === 'RUNNING') {
            statusHTML += '<tr class = "success">';
            statusHTML += '<td>'+ commpoints.data.id + '</td>';
            statusHTML += '<td>'+ commpoints.data.name + '</td>';
            statusHTML += '<td>'+ commpoints.data.outQueueSize + '</td>';
            statusHTML += '<td>' + commpoints.data.receivedCount + '</td>';
            statusHTML += '<td>' + commpoints.data.sentCount + '</td>';
            statusHTML += '<td>' + commpoints.data.state + '</td>';

            statusHTML += '<td>' +
          '<button class="btnnn btn-warning">RESTART</button>'
          + '</td>';

            running = '<button type = "button"  class="butnn btn-success ">' + commpoints.data.state + '</button>';
            console.log(statusHTML);
          } else if(commpoints.data.state === 'RETRYING') {
            statusHTML += '<tr class = "warning">';
            statusHTML += '<td>'+ commpoints.data.id + '</td>';
            statusHTML += '<td>'+ commpoints.data.name + '</td>';
            statusHTML += '<td>'+ commpoints.data.outQueueSize + '</td>';
            statusHTML += '<td>' + commpoints.data.receivedCount + '</td>';
            statusHTML += '<td>' + commpoints.data.sentCount + '</td>';
            statusHTML += '<td>' + commpoints.data.state + '</td>';

            statusHTML += '<td>' +
            '<button class="btnnn btn-warning">RESTART</button>'
            + '</td>';

            retrying = '<button type = "button"  class="butnn btn-warning ">' + commpoints.data.state + '</button>';
          } else if(commpoints.data.state === 'STOPPED') {
            statusHTML += '<tr class = "danger">';
            statusHTML += '<td>'+ commpoints.data.id + '</td>';
            statusHTML += '<td>'+ commpoints.data.name + '</td>';
            statusHTML += '<td>'+ commpoints.data.outQueueSize + '</td>';
            statusHTML += '<td>' + commpoints.data.receivedCount + '</td>';
            statusHTML += '<td>' + commpoints.data.sentCount + '</td>';
            statusHTML += '<td>' + commpoints.data.state + '</td>';
            
            statusHTML += '<td>' +
            '<button class="btnnn btn-warning">RESTART</button>'
            + '</td>';

            numb_stop[1] = 1;
            stopped = '<button type = "button"  class="butnn btn-danger ">' + Count_stopped() + '</button>';
            /*commpoints_stopped(commpoints.data.state, commpoints.data.name);*/
            
          }else if(commpoints.data.state === 'ERROR'){
            statusHTML += '<tr class = "danger">';
            statusHTML += '<td>'+ commpoints.data.id + '</td>';
            statusHTML += '<td>'+ commpoints.data.name + '</td>';
            statusHTML += '<td>'+ commpoints.data.outQueueSize + '</td>';
            statusHTML += '<td>' + commpoints.data.receivedCount + '</td>';
            statusHTML += '<td>' + commpoints.data.sentCount + '</td>';
            statusHTML += '<td>' + commpoints.data.state + '</td>';
            statusHTML += '<td>' +
            '<button class="btnnn btn-warning">RESTART</button>'
            + '</td>';
            statusHTML += '<td>' +
            '<button class="btnnn btn-warning">RESTART</button>'
            + '</td>';

            numb_error[1] = 1;
            error = '<button type = "button"  class="butnn btn-danger ">' + Count_error() + '</button>';
           /* commpoints_error( commpoints.data.state, commpoints.data.name );*/
          }
          statusHTML += '</tr>';
    
          document.querySelector('.commstatus_1 tbody.comm_2').innerHTML = statusHTML;
          document.querySelector('.commstatus_2 tbody.comm_2').innerHTML = statusHTML;
          document.querySelector('.commstatus_3 tbody.comm_2').innerHTML = statusHTML;
          document.querySelector('.commstatus_4 tbody.comm_2').innerHTML = statusHTML;
          if(error !== null && error !== undefined && (error !== ""))
          {
            document.querySelector('#target').innerHTML = error;
            document.querySelector('#target_1').innerHTML = error;
            document.querySelector('#target_2').innerHTML = error;
            document.querySelector('#target_3').innerHTML = error;
          }else if(retrying !== null && retrying !== undefined && (retrying !== ""))
          {
          document.querySelector('#target').innerHTML = retrying;
          document.querySelector('#target_1').innerHTML = retrying;
          document.querySelector('#target_2').innerHTML = retrying;
          document.querySelector('#target_3').innerHTML = retrying;
          }else if(stopped !== null && stopped !== undefined && (stopped !== ""))
          {
          document.querySelector('#target').innerHTML = stopped;
          document.querySelector('#target_1').innerHTML = stopped;
          document.querySelector('#target_2').innerHTML = stopped;
          document.querySelector('#target_3').innerHTML = stopped;
        
          }else if(running !== null && running !== undefined && (running !== "")){
            document.querySelector('#target').innerHTML = running;
            document.querySelector('#target_1').innerHTML = running;
            document.querySelector('#target_2').innerHTML = running;
            document.querySelector('#target_3').innerHTML = running;
          }else {
            var info = '';
            info = '<button type = "button"  class="butnn btn-danger " valid = "NO ERROR">'  + '</button>';
          }
          interval = setTimeout(callcommpoints_1, 30000);
      }
    }
    
        xhttp_one.open('GET', commp, true);
        xhttp_one.send();
    
    }
    callcommpoints_2(); 


    function commpoints_error(co, name) {
    var text = "Monitoring Status" + "\n" + "\n" + "Name:" + name + "\n" + "TimeStamp:" + Time_get() + "\n" +"Type:" + "Routes" + "\n" + "Status:" + co;
    var blob = new Blob([text], {type: "text/plain;charset=utf-8"});
    saveAs(blob, filename);
  };

  function Time_get() {
    var today = new Date();
    var y = today.getFullYear();
    // JavaScript months are 0-based.
    var m = today.getMonth() + 1;
    var d = today.getDate();
    var h = today.getHours();
    var mi = today.getMinutes();
    var s = today.getSeconds();
    return (y + "/" + m + "/" + d + "  "+ h + ":" + mi + ":" + s);
}


function Count_error(){
  return(numb_error[0] + numb_error[1]);
}