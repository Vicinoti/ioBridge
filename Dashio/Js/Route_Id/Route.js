const num_error = Array(30);
for (let q = 0; q < num_error.length; q++) {
    num_error[q] = 0;
}
const num_stop = Array(30);
for (let p = 0; p < num_stop.length; p++) {
    num_stop[p] = 0;
}

/*"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --disable-web-security --disable-gpu --user-data-dir=~/chromeTemp*/
var running = '';
var error = '';
var retrying = '';
var stopped = '';

function description() {
  var stat = new XMLHttpRequest();
  var comm = [];
  stat.onload = function() {
  if(stat.readyState === 4) {
  var route = JSON.parse(stat.responseText);
    for(var i = 0; i < route.Workflows.length; i++){
      for(var j = 0; j < route.Workflows[i].Routes.length; j++){
        comm[j] = route.Workflows[i].Routes[j].API;
        
        if(j === 0){
          console.log(comm[j]);
          callroute_1(comm[j]);
        }
        if(j === 1){
          console.log(comm[j]);
          callroute_2(comm[j]);
        } 
        if(j === 2){
          console.log(comm[j]);
          callroute_3(comm[j]);
        }
        if(j === 3){
          console.log(comm[j]);
          callroute_4(comm[j]);
        } 
        if(j === 4){
          console.log(comm[j]);
          callroute_5(comm[j]);
        }
        if(j === 5){
          console.log(comm[j]);
          callroute_6(comm[j]);
        } 
        if(j === 6){
          console.log(comm[j]);
          callroute_7(comm[j]);
        }
        if(j === 7){
          console.log(comm[j]);
          callroute_8(comm[j]);
        }
        if(j === 8){
          console.log(comm[j]);
          callroute_9(comm[j]);
        }
        if(j === 9){
          console.log(comm[j]);
          callroute_10(comm[j]);
        }
        if(j === 10){
          console.log(comm[j]);
          callroute_11(comm[j]);
        } 
        if(j === 11){
          console.log(comm[j]);
          callroute_12(comm[j]);
        }
        if(j === 12){
          console.log(comm[j]);
          callroute_13(comm[j]);
        }
        if(j === 13){
          console.log(comm[j]);
          callroute_14(comm[j]);
        } 
    }
  }
}
    interval = setTimeout(description, 30000);
  }
stat.open('GET', 'Json/JSON Example.json');
stat.send();
}
description();

function callroute_1(comm) {
  var xhttp_one = new XMLHttpRequest();
  
  xhttp_one.onreadystatechange = function() {
    if(xhttp_one.readyState === 4 ) {
      var route = JSON.parse(xhttp_one.responseText);
      console.log(route);
      
      var statusHTML = '';
      var running = '';
      var error = '';
      var retrying = '';
      statusHTML += '<tr>';
      if(route.data.state === 'RUNNING') {
        statusHTML += '<tr class = "success">';
        statusHTML += '<td>'+ route.data.id + '</td>';
        statusHTML += '<td>'+ route.data.name + '</td>';
        statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
        statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
        statusHTML += '<td>' + route.data.processedCount + '</td>';
        statusHTML += '<td>' + route.data.state + '</td>';

        statusHTML += '<td>' +
        '<button class="btnnn btn-warning">RESTART</button>'
        + '</td>';
        console.log(statusHTML);
      } else if(route.data.state === 'RETRYING') {
        statusHTML += '<tr class = "warning">';
        statusHTML += '<td>'+ route.data.id + '</td>';
        statusHTML += '<td>'+ route.data.name + '</td>';
        statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
        statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
        statusHTML += '<td>' + route.data.processedCount + '</td>';
        statusHTML += '<td>' + route.data.state + '</td>';
        statusHTML += '<td>' +
        '<button class="btnnn btn-warning">RESTART</button>'
        + '</td>';
        retrying = '<button type = "button"  class="butnn btn-warning ">' + route.data.state + '</button>';
    
      } else if(route.data.state === 'STOPPED') {
        statusHTML += '<tr class = "danger">';
        statusHTML += '<td>'+ route.data.id + '</td>';
        statusHTML += '<td>'+ route.data.name + '</td>';
        statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
        statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
        statusHTML += '<td>' + route.data.processedCount + '</td>';
        statusHTML += '<td>' + route.data.state + '</td>';
        statusHTML += '<td>' +
        '<button class="btnnn btn-warning">RESTART</button>'
        + '</td>';
        num_stop[0] = 1;
        stopped = '<button type = "button"  class="butnn btn-danger ">' + count_stopped() + '</button>';
        route_stopped(route.data.state, route.data.name);

      }else if(route.data.state === 'ERROR'){
        statusHTML += '<tr class = "danger">';
        statusHTML += '<td>'+ route.data.id + '</td>';
        statusHTML += '<td>'+ route.data.name + '</td>';
        statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
        statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
        statusHTML += '<td>' + route.data.processedCount + '</td>';
        statusHTML += '<td>' + route.data.state + '</td>';
        statusHTML += '<td>' +
        '<button class="btnnn btn-warning">RESTART</button>'
        + '</td>';
        num_error[0] = 1;
        error = '<button type = "button"  class="butnn btn-danger ">' + count_error() + '</button>';
        route_error(route.data.state, route.data.name );
      }
      
      statusHTML += '</tr>';
  
        document.querySelector('.routestatus_1 tbody.route_1').innerHTML = statusHTML;
        document.querySelector('.routestatus_2 tbody.route_1').innerHTML = statusHTML;
        document.querySelector('.routestatus_3 tbody.route_1').innerHTML = statusHTML;
        document.querySelector('.routestatus_4 tbody.route_1').innerHTML = statusHTML;
        if(error !== null && error !== undefined && (error !== ""))
          {
            document.querySelector('#STATUS ').innerHTML = error;
            document.querySelector('#STATUS_1 ').innerHTML = error;
            document.querySelector('#STATUS_2 ').innerHTML = error;
            document.querySelector('#STATUS_3 ').innerHTML = error;
          }else if(retrying !== null && retrying !== undefined && (retrying !== ""))
          {
          document.querySelector('#STATUS').innerHTML = retrying;
          document.querySelector('#STATUS_1').innerHTML = retrying;
          document.querySelector('#STATUS_2').innerHTML = retrying;
          document.querySelector('#STATUS_3').innerHTML = retrying;
          }else if(stopped !== null && stopped !== undefined && (stopped !== ""))
          {
          document.querySelector('#STATUS').innerHTML = stopped;
          document.querySelector('#STATUS_1').innerHTML = stopped;
          document.querySelector('#STATUS_2').innerHTML = stopped;
          document.querySelector('#STATUS_3').innerHTML = stopped;
          }else if(running !== null && running !== undefined && (running !== "")){
            document.querySelector('#STATUS ').innerHTML = running;
            document.querySelector('#STATUS_1 ').innerHTML = running;
            document.querySelector('#STATUS_2 ').innerHTML = running;
            document.querySelector('#STATUS_3 ').innerHTML = running;
          }else {
            var info = '';
            info = '<button type = "button"  class="butnn btn-danger " value = "NO ERROR">'  + '</button>';
          }

        interval = setTimeout(callroute_1, 30000);
    }
  }
  
      xhttp_one.open('GET', comm, true);
      xhttp_one.send();
  
  }
  callroute_1(); 


  function callroute_2(comm) {
    var xhttp_one = new XMLHttpRequest();
    
    xhttp_one.onreadystatechange = function() {
      if(xhttp_one.readyState === 4 ) {
        var route = JSON.parse(xhttp_one.responseText);
        console.log(route);
        
        var statusHTML = '';
        var error = '';
      var retrying = '';
      var running = '';
          statusHTML += '<tr>';
          if(route.data.state === 'RUNNING') {
            statusHTML += '<tr class = "success">';
            statusHTML += '<td>'+ route.data.id + '</td>';
            statusHTML += '<td>'+ route.data.name + '</td>';
            statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
            statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
            statusHTML += '<td>' + route.data.processedCount + '</td>';
            statusHTML += '<td>' + route.data.state + '</td>';
            statusHTML += '<td>' +
            '<button class="btnnn btn-warning">RESTART</button>'
            + '</td>';
            console.log(statusHTML);
          } else if(route.data.state === 'RETRYING') {
            statusHTML += '<tr class = "warning">';
            statusHTML += '<td>'+ route.data.id + '</td>';
            statusHTML += '<td>'+ route.data.name + '</td>';
            statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
            statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
            statusHTML += '<td>' + route.data.processedCount + '</td>';
            statusHTML += '<td>' + route.data.state + '</td>';
            statusHTML += '<td>' +
            '<button class="btnnn btn-warning">RESTART</button>'
            + '</td>';
            retrying = '<button type = "button"  class="butnn btn-warning ">' + route.data.state + '</button>';;
          } else if(route.data.state === 'STOPPED') {
            statusHTML += '<tr class = "danger">';
            statusHTML += '<td>'+ route.data.id + '</td>';
            statusHTML += '<td>'+ route.data.name + '</td>';
            statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
            statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
            statusHTML += '<td>' + route.data.processedCount + '</td>';
            statusHTML += '<td>' + route.data.state + '</td>';
            statusHTML += '<td>' +
            '<button class="btnnn btn-warning">RESTART</button>'
            + '</td>';
  
        num_stop[1] = 1;
        stopped = '<button type = "button"  class="butnn btn-danger ">' + count_stopped() + '</button>';
        route_stopped(route.data.state, route.data.name);
  
          }else if(route.data.state === 'ERROR'){
            statusHTML += '<tr class = "danger">';
            statusHTML += '<td>'+ route.data.id + '</td>';
            statusHTML += '<td>'+ route.data.name + '</td>';
            statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
            statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
            statusHTML += '<td>' + route.data.processedCount + '</td>';
            statusHTML += '<td>' + route.data.state + '</td>';
            statusHTML += '<td>' +
            '<button class="btnnn btn-warning">RESTART</button>'
            + '</td>';
  
            num_error[1] = 1;
            error = '<button type = "button"  class="butnn btn-danger ">' + count_error() + '</button>';
            route_error(route.data.state, route.data.name );
          }
          statusHTML += '</tr>';
    
          document.querySelector('.routestatus_1 tbody.route_2').innerHTML = statusHTML;
          document.querySelector('.routestatus_2 tbody.route_2').innerHTML = statusHTML;
          document.querySelector('.routestatus_3 tbody.route_2').innerHTML = statusHTML;
          document.querySelector('.routestatus_4 tbody.route_2').innerHTML = statusHTML;
          if(error !== null && error !== undefined && (error !== ""))
          {
            document.querySelector('#STATUS ').innerHTML = error;
            document.querySelector('#STATUS_1 ').innerHTML = error;
            document.querySelector('#STATUS_2 ').innerHTML = error;
            document.querySelector('#STATUS_3 ').innerHTML = error;
          }else if(retrying !== null && retrying !== undefined && (retrying !== ""))
          {
          document.querySelector('#STATUS').innerHTML = retrying;
          document.querySelector('#STATUS_1').innerHTML = retrying;
          document.querySelector('#STATUS_2').innerHTML = retrying;
          document.querySelector('#STATUS_3').innerHTML = retrying;
          }else if(stopped !== null && stopped !== undefined && (stopped !== ""))
          {
          document.querySelector('#STATUS').innerHTML = stopped;
          document.querySelector('#STATUS_1').innerHTML = stopped;
          document.querySelector('#STATUS_2').innerHTML = stopped;
          document.querySelector('#STATUS_3').innerHTML = stopped;
          }else if(running !== null && running !== undefined && (running !== "")){
            document.querySelector('#STATUS ').innerHTML = running;
            document.querySelector('#STATUS_1 ').innerHTML = running;
            document.querySelector('#STATUS_2 ').innerHTML = running;
            document.querySelector('#STATUS_3 ').innerHTML = running;
          }else {
            var info = '';
            info = '<button type = "button"  class="butnn btn-danger " value = "NO ERROR">'  + '</button>';
          }
          interval = setTimeout(callroute_1, 30000);
      }
    }
    
        xhttp_one.open('GET', comm, true);
        xhttp_one.send();
    
    }
    callroute_2(); 

    function callroute_3(comm) {
      var xhttp_one = new XMLHttpRequest();
      
      xhttp_one.onreadystatechange = function() {
        if(xhttp_one.readyState === 4 ) {
          var route = JSON.parse(xhttp_one.responseText);
          console.log(route);
          
          var statusHTML = '';
          var error = '';
          var retrying = '';
          var running = '';
            statusHTML += '<tr>';
            if(route.data.state === 'RUNNING') {
              statusHTML += '<tr class = "success">';
              statusHTML += '<td>'+ route.data.id + '</td>';
              statusHTML += '<td>'+ route.data.name + '</td>';
              statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
              statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
              statusHTML += '<td>' + route.data.processedCount + '</td>';
              statusHTML += '<td>' + route.data.state + '</td>';
              statusHTML += '<td>' +
              '<button class="btnnn btn-warning">RESTART</button>'
              + '</td>';
              console.log(statusHTML);
            } else if(route.data.state === 'RETRYING') {
              statusHTML += '<tr class = "warning">';
              statusHTML += '<td>'+ route.data.id + '</td>';
              statusHTML += '<td>'+ route.data.name + '</td>';
              statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
              statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
              statusHTML += '<td>' + route.data.processedCount + '</td>';
              statusHTML += '<td>' + route.data.state + '</td>';
              statusHTML += '<td>' +
              '<button class="btnnn btn-warning">RESTART</button>'
              + '</td>';
              retrying = '<button type = "button"  class="butnn btn-warning ">' + route.data.state + '</button>';
            } else if(route.data.state === 'STOPPED') {
              statusHTML += '<tr class = "danger">';
              statusHTML += '<td>'+route.data.id + '</td>';
              statusHTML += '<td>'+ route.data.name + '</td>';
              statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
              statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
              statusHTML += '<td>' + route.data.processedCount + '</td>';
              statusHTML += '<td>' + route.data.state + '</td>';
              statusHTML += '<td>' +
              '<button class="btnnn btn-warning">RESTART</button>'
              + '</td>';
              num_stop[2] = 1;
              stopped = '<button type = "button"  class="butnn btn-danger ">' + count_stopped() + '</button>';
              route_stopped(route.data.state, route.data.name);
    
            }else if(route.data.state === 'ERROR'){
              statusHTML += '<tr class = "danger">';
              statusHTML += '<td>'+ route.data.id + '</td>';
              statusHTML += '<td>'+ route.data.name + '</td>';
              statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
              statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
              statusHTML += '<td>' + route.data.processedCount + '</td>';
              statusHTML += '<td>' + route.data.state + '</td>';
              statusHTML += '<td>' +
              '<button class="btnnn btn-warning">RESTART</button>'
              + '</td>';
              num_error[2] = 1;
              error = '<button type = "button"  class="butnn btn-danger ">' + count_error() + '</button>';
              route_error(route.data.state, route.data.name );
            }
            statusHTML += '</tr>';
      
            document.querySelector('.routestatus_1 tbody.route_3').innerHTML = statusHTML;
            document.querySelector('.routestatus_2 tbody.route_3').innerHTML = statusHTML;
            document.querySelector('.routestatus_3 tbody.route_3').innerHTML = statusHTML;
            document.querySelector('.routestatus_4 tbody.route_3').innerHTML = statusHTML;
            if(error !== null && error !== undefined && (error !== ""))
          {
            document.querySelector('#STATUS ').innerHTML = error;
            document.querySelector('#STATUS_1 ').innerHTML = error;
            document.querySelector('#STATUS_2 ').innerHTML = error;
            document.querySelector('#STATUS_3 ').innerHTML = error;
          }else if(retrying !== null && retrying !== undefined && (retrying !== ""))
          {
          document.querySelector('#STATUS').innerHTML = retrying;
          document.querySelector('#STATUS_1').innerHTML = retrying;
          document.querySelector('#STATUS_2').innerHTML = retrying;
          document.querySelector('#STATUS_3').innerHTML = retrying;
          }else if(stopped !== null && stopped !== undefined && (stopped !== ""))
          {
          document.querySelector('#STATUS').innerHTML = stopped;
          document.querySelector('#STATUS_1').innerHTML = stopped;
          document.querySelector('#STATUS_2').innerHTML = stopped;
          document.querySelector('#STATUS_3').innerHTML = stopped;
          }else if(running !== null && running !== undefined && (running !== "")){
            document.querySelector('#STATUS ').innerHTML = running;
            document.querySelector('#STATUS_1 ').innerHTML = running;
            document.querySelector('#STATUS_2 ').innerHTML = running;
            document.querySelector('#STATUS_3 ').innerHTML = running;
          }else {
            var info = '';
            info = '<button type = "button"  class="butnn btn-danger " value = "NO ERROR">'  + '</button>';
          }
            interval = setTimeout(callroute_1, 30000);
        }
      }
      
          xhttp_one.open('GET', comm, true);
          xhttp_one.send();
      
      }
      callroute_3(); 

      function callroute_4(comm) {
        var xhttp_one = new XMLHttpRequest();
        
        xhttp_one.onreadystatechange = function() {
          if(xhttp_one.readyState === 4 ) {
            var route = JSON.parse(xhttp_one.responseText);
            console.log(route);
            
            var statusHTML = '';
            var error = '';
            var retrying = '';
            var running = '';
              statusHTML += '<tr>';
              if(route.data.state === 'RUNNING') {
                statusHTML += '<tr class = "success">';
                statusHTML += '<td>'+ route.data.id + '</td>';
                statusHTML += '<td>'+ route.data.name + '</td>';
                statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                statusHTML += '<td>' + route.data.processedCount + '</td>';
                statusHTML += '<td>' + route.data.state + '</td>';
                statusHTML += '<td>' +
                '<button class="btnnn btn-warning">RESTART</button>'
                + '</td>';
                console.log(statusHTML);
              } else if(route.data.state === 'RETRYING') {
                statusHTML += '<tr class = "warning">';
                statusHTML += '<td>'+ route.data.id + '</td>';
                statusHTML += '<td>'+ route.data.name + '</td>';
                statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                statusHTML += '<td>' + route.data.processedCount + '</td>';
                statusHTML += '<td>' + route.data.state + '</td>';
                statusHTML += '<td>' +
                '<button class="btnnn btn-warning">RESTART</button>'
                + '</td>';
                retrying = '<button type = "button"  class="butnn btn-warning ">' + route.data.state + '</button>';
              } else if(route.data.state === 'STOPPED') {
                statusHTML += '<tr class = "danger">';
                statusHTML += '<td>'+route.data.id + '</td>';
                statusHTML += '<td>'+ route.data.name + '</td>';
                statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                statusHTML += '<td>' + route.data.processedCount + '</td>';
                statusHTML += '<td>' + route.data.state + '</td>';
                statusHTML += '<td>' +
                '<button class="btnnn btn-warning">RESTART</button>'
                + '</td>';
                num_stop[3] = 1;
                stopped = '<button type = "button"  class="butnn btn-danger ">' + count_stopped() + '</button>';
                route_stopped(route.data.state, route.data.name);
      
              }else if(route.data.state === 'ERROR'){
                statusHTML += '<tr class = "danger">';
                statusHTML += '<td>'+ route.data.id + '</td>';
                statusHTML += '<td>'+ route.data.name + '</td>';
                statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                statusHTML += '<td>' + route.data.processedCount + '</td>';
                statusHTML += '<td>' + route.data.state + '</td>';
                statusHTML += '<td>' +
                '<button class="btnnn btn-warning">RESTART</button>'
                + '</td>';
                num_error[3] = 1;
                error = '<button type = "button"  class="butnn btn-danger ">' + count_error() + '</button>';
                route_error(route.data.state, route.data.name );
              }
              statusHTML += '</tr>';
        
              document.querySelector('.routestatus_1 tbody.route_4').innerHTML = statusHTML;
              document.querySelector('.routestatus_2 tbody.route_4').innerHTML = statusHTML;
              document.querySelector('.routestatus_3 tbody.route_4').innerHTML = statusHTML;
              document.querySelector('.routestatus_4 tbody.route_4').innerHTML = statusHTML;
              if(error !== null && error !== undefined && (error !== ""))
          {
            document.querySelector('#STATUS ').innerHTML = error;
            document.querySelector('#STATUS_1 ').innerHTML = error;
            document.querySelector('#STATUS_2 ').innerHTML = error;
            document.querySelector('#STATUS_3 ').innerHTML = error;
          }else if(retrying !== null && retrying !== undefined && (retrying !== ""))
          {
          document.querySelector('#STATUS').innerHTML = retrying;
          document.querySelector('#STATUS_1').innerHTML = retrying;
          document.querySelector('#STATUS_2').innerHTML = retrying;
          document.querySelector('#STATUS_3').innerHTML = retrying;
          }else if(stopped !== null && stopped !== undefined && (stopped !== ""))
          {
          document.querySelector('#STATUS').innerHTML = stopped;
          document.querySelector('#STATUS_1').innerHTML = stopped;
          document.querySelector('#STATUS_2').innerHTML = stopped;
          document.querySelector('#STATUS_3').innerHTML = stopped;
          }else if(running !== null && running !== undefined && (running !== "")){
            document.querySelector('#STATUS ').innerHTML = running;
            document.querySelector('#STATUS_1 ').innerHTML = running;
            document.querySelector('#STATUS_2 ').innerHTML = running;
            document.querySelector('#STATUS_3 ').innerHTML = running;
          }else {
            var info = '';
            info = '<button type = "button"  class="butnn btn-danger " value = "NO ERROR">'  + '</button>';
          }
              interval = setTimeout(callroute_1, 30000);
          }
        }
        
            xhttp_one.open('GET', comm, true);
            xhttp_one.send();
        
        }
        callroute_4(); 


          function callroute_5(comm) {
            var xhttp_one = new XMLHttpRequest();
            
            xhttp_one.onreadystatechange = function() {
              if(xhttp_one.readyState === 4 ) {
                var route = JSON.parse(xhttp_one.responseText);
                console.log(route);
                
                var statusHTML = '';
                var error = '';
      var retrying = '';
      var running = '';
                  statusHTML += '<tr>';
                  if(route.data.state === 'RUNNING') {
                    statusHTML += '<tr class = "success">';
                    statusHTML += '<td>'+ route.data.id + '</td>';
                    statusHTML += '<td>'+ route.data.name + '</td>';
                    statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                    statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                    statusHTML += '<td>' + route.data.processedCount + '</td>';
                    statusHTML += '<td>' + route.data.state + '</td>';
                    statusHTML += '<td>' +
                    '<button class="btnnn btn-warning">RESTART</button>'
                    + '</td>';
                    console.log(statusHTML);
                  } else if(route.data.state === 'RETRYING') {
                    statusHTML += '<tr class = "warning">';
                    statusHTML += '<td>'+ route.data.id + '</td>';
                    statusHTML += '<td>'+ route.data.name + '</td>';
                    statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                    statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                    statusHTML += '<td>' + route.data.processedCount + '</td>';
                    statusHTML += '<td>' + route.data.state + '</td>';
                    statusHTML += '<td>' +
                    '<button class="btnnn btn-warning">RESTART</button>'
                    + '</td>';
                    retrying = '<button type = "button"  class="butnn btn-warning ">' + route.data.state + '</button>';
                  } else if(route.data.state === 'STOPPED') {
                    statusHTML += '<tr class = "danger">';
                    statusHTML += '<td>'+ route.data.id + '</td>';
                    statusHTML += '<td>'+ route.data.name + '</td>';
                    statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                    statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                    statusHTML += '<td>' + route.data.processedCount + '</td>';
                    statusHTML += '<td>' + route.data.state + '</td>';
                    statusHTML += '<td>' +
                    '<button class="btnnn btn-warning">RESTART</button>'
                    + '</td>';
          
                    num_stop[4] = 1;
                    stopped = '<button type = "button"  class="butnn btn-danger ">' + count_stopped() + '</button>';
                    route_stopped(route.data.state, route.data.name);
          
                  }else if(route.data.state === 'ERROR'){
                    statusHTML += '<tr class = "danger">';
                    statusHTML += '<td>'+route.data.id + '</td>';
                    statusHTML += '<td>'+ route.data.name + '</td>';
                    statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                    statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                    statusHTML += '<td>' + route.data.processedCount + '</td>';
                    statusHTML += '<td>' + route.data.state + '</td>';
                    statusHTML += '<td>' +
                    '<button class="btnnn btn-warning">RESTART</button>'
                    + '</td>';
          
                    num_error[4] = 1;
                    error = '<button type = "button"  class="butnn btn-danger ">' + count_error() + '</button>';
                    route_error(route.data.state, route.data.name );
                  }
                  statusHTML += '</tr>';
            
                  document.querySelector('.routestatus_1 tbody.route_5').innerHTML = statusHTML;
                  document.querySelector('.routestatus_2 tbody.route_5').innerHTML = statusHTML;
                  document.querySelector('.routestatus_3 tbody.route_5').innerHTML = statusHTML;
                  document.querySelector('.routestatus_4 tbody.route_5').innerHTML = statusHTML;
                  if(error !== null && error !== undefined && (error !== ""))
          {
            document.querySelector('#STATUS ').innerHTML = error;
            document.querySelector('#STATUS_1 ').innerHTML = error;
            document.querySelector('#STATUS_2 ').innerHTML = error;
            document.querySelector('#STATUS_3 ').innerHTML = error;
          }else if(retrying !== null && retrying !== undefined && (retrying !== ""))
          {
          document.querySelector('#STATUS').innerHTML = retrying;
          document.querySelector('#STATUS_1').innerHTML = retrying;
          document.querySelector('#STATUS_2').innerHTML = retrying;
          document.querySelector('#STATUS_3').innerHTML = retrying;
          }else if(stopped !== null && stopped !== undefined && (stopped !== ""))
          {
          document.querySelector('#STATUS').innerHTML = stopped;
          document.querySelector('#STATUS_1').innerHTML = stopped;
          document.querySelector('#STATUS_2').innerHTML = stopped;
          document.querySelector('#STATUS_3').innerHTML = stopped;
          }else if(running !== null && running !== undefined && (running !== "")){
            document.querySelector('#STATUS ').innerHTML = running;
            document.querySelector('#STATUS_1 ').innerHTML = running;
            document.querySelector('#STATUS_2 ').innerHTML = running;
            document.querySelector('#STATUS_3 ').innerHTML = running;
          }else {
            var info = '';
            info = '<button type = "button"  class="butnn btn-danger " value = "NO ERROR">'  + '</button>';
          }
                  interval = setTimeout(callroute_1, 30000);
              }
            }
            
                xhttp_one.open('GET', comm, true);
                xhttp_one.send();
            
            }
            callroute_5(); 

            function callroute_6(comm) {
              var xhttp_one = new XMLHttpRequest();
              
              xhttp_one.onreadystatechange = function() {
                if(xhttp_one.readyState === 4 ) {
                  var route = JSON.parse(xhttp_one.responseText);
                  console.log(route);
                  
                  var statusHTML = '';
                  var error = '';
                  var retrying = '';
                  var running = '';
                    statusHTML += '<tr>';
                    if(route.data.state === 'RUNNING') {
                      statusHTML += '<tr class = "success">';
                      statusHTML += '<td>'+ route.data.id + '</td>';
                      statusHTML += '<td>'+ route.data.name + '</td>';
                      statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                      statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                      statusHTML += '<td>' + route.data.processedCount + '</td>';
                      statusHTML += '<td>' + route.data.state + '</td>';
                      statusHTML += '<td>' +
                      '<button class="btnnn btn-warning">RESTART</button>'
                      + '</td>';
                      console.log(statusHTML);
                    } else if(route.data.state === 'RETRYING') {
                      statusHTML += '<tr class = "warning">';
                      statusHTML += '<td>'+ route.data.id + '</td>';
                      statusHTML += '<td>'+ route.data.name + '</td>';
                      statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                      statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                      statusHTML += '<td>' + route.data.processedCount + '</td>';
                      statusHTML += '<td>' + route.data.state + '</td>';
                      statusHTML += '<td>' +
                      '<button class="btnnn btn-warning">RESTART</button>'
                      + '</td>';
                      retrying = '<button type = "button"  class="butnn btn-warning ">' + route.data.state + '</button>';
                    } else if(route.data.state === 'STOPPED') {
                      statusHTML += '<tr class = "danger">';
                      statusHTML += '<td>'+ route.data.id + '</td>';
                      statusHTML += '<td>'+ route.data.name + '</td>';
                      statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                      statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                      statusHTML += '<td>' + route.data.processedCount + '</td>';
                      statusHTML += '<td>' + route.data.state + '</td>';
                      statusHTML += '<td>' +
                      '<button class="btnnn btn-warning">RESTART</button>'
                      + '</td>';
                      num_stop[5] = 1;
                      stopped = '<button type = "button"  class="butnn btn-danger ">' + count_stopped() + '</button>';
                      route_stopped(route.data.state, route.data.name);
            
                    }else if(route.data.state === 'ERROR'){
                      statusHTML += '<tr class = "danger">';
                      statusHTML += '<td>'+ route.data.id + '</td>';
                      statusHTML += '<td>'+ route.data.name + '</td>';
                      statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                      statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                      statusHTML += '<td>' + route.data.processedCount + '</td>';
                      statusHTML += '<td>' + route.data.state + '</td>';
                      statusHTML += '<td>' +
                      '<button class="btnnn btn-warning">RESTART</button>'
                      + '</td>';
                      num_error[5] = 1;
                      error = '<button type = "button"  class="butnn btn-danger ">' + count_error() + '</button>';
                      route_error(route.data.state, route.data.name );
                    }
                    statusHTML += '</tr>';
              
                    document.querySelector('.routestatus_1 tbody.route_6').innerHTML = statusHTML;
                    document.querySelector('.routestatus_2 tbody.route_6').innerHTML = statusHTML;
                    document.querySelector('.routestatus_3 tbody.route_6').innerHTML = statusHTML;
                    document.querySelector('.routestatus_4 tbody.route_6').innerHTML = statusHTML;
                    if(error !== null && error !== undefined && (error !== ""))
          {
            document.querySelector('#STATUS ').innerHTML = error;
            document.querySelector('#STATUS_1 ').innerHTML = error;
            document.querySelector('#STATUS_2 ').innerHTML = error;
            document.querySelector('#STATUS_3 ').innerHTML = error;
          }else if(retrying !== null && retrying !== undefined && (retrying !== ""))
          {
          document.querySelector('#STATUS').innerHTML = retrying;
          document.querySelector('#STATUS_1').innerHTML = retrying;
          document.querySelector('#STATUS_2').innerHTML = retrying;
          document.querySelector('#STATUS_3').innerHTML = retrying;
          }else if(stopped !== null && stopped !== undefined && (stopped !== ""))
          {
          document.querySelector('#STATUS').innerHTML = stopped;
          document.querySelector('#STATUS_1').innerHTML = stopped;
          document.querySelector('#STATUS_2').innerHTML = stopped;
          document.querySelector('#STATUS_3').innerHTML = stopped;
          }else if(running !== null && running !== undefined && (running !== "")){
            document.querySelector('#STATUS ').innerHTML = running;
            document.querySelector('#STATUS_1 ').innerHTML = running;
            document.querySelector('#STATUS_2 ').innerHTML = running;
            document.querySelector('#STATUS_3 ').innerHTML = running;
          }else {
            var info = '';
            info = '<button type = "button"  class="butnn btn-danger " value = "NO ERROR">'  + '</button>';
          }
                    interval = setTimeout(callroute_1, 30000);
                }
              }
              
                  xhttp_one.open('GET', comm, true);
                  xhttp_one.send();
              
              }
              callroute_6(); 

              function callroute_7(comm) {
                var xhttp_one = new XMLHttpRequest();
                
                xhttp_one.onreadystatechange = function() {
                  if(xhttp_one.readyState === 4 ) {
                    var route = JSON.parse(xhttp_one.responseText);
                    console.log(route);
                    
                    var statusHTML = '';
                    var error = '';
                    var retrying = '';
                    var running = '';
                      statusHTML += '<tr>';
                      if(route.data.state === 'RUNNING') {
                        statusHTML += '<tr class = "success">';
                        statusHTML += '<td>'+ route.data.id + '</td>';
                        statusHTML += '<td>'+ route.data.name + '</td>';
                        statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                        statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                        statusHTML += '<td>' + route.data.processedCount + '</td>';
                        statusHTML += '<td>' + route.data.state + '</td>';
                        console.log(statusHTML);
                      } else if(route.data.state === 'RETRYING') {
                        statusHTML += '<tr class = "warning">';
                        statusHTML += '<td>'+ route.data.id + '</td>';
                        statusHTML += '<td>'+ route.data.name + '</td>';
                        statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                        statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                        statusHTML += '<td>' + route.data.processedCount + '</td>';
                        statusHTML += '<td>' + route.data.state + '</td>';
                        retrying = '<button type = "button"  class="butnn btn-warning ">' + route.data.state + '</button>';
                      } else if(route.data.state === 'STOPPED') {
                        statusHTML += '<tr class = "danger">';
                        statusHTML += '<td>'+ route.data.id + '</td>';
                        statusHTML += '<td>'+ route.data.name + '</td>';
                        statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                        statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                        statusHTML += '<td>' + route.data.processedCount + '</td>';
                        statusHTML += '<td>' + route.data.state + '</td>';
                        num_stop[6] = 1;
                        stopped = '<button type = "button"  class="butnn btn-danger ">' + count_stopped() + '</button>';
                        route_stopped(route.data.state, route.data.name);
              
                      }else if(route.data.state === 'ERROR'){
                        statusHTML += '<tr class = "danger">';
                        statusHTML += '<td>'+ route.data.id + '</td>';
                        statusHTML += '<td>'+ route.data.name + '</td>';
                        statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                        statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                        statusHTML += '<td>' + route.data.processedCount + '</td>';
                        statusHTML += '<td>' + route.data.state + '</td>';
              
                        num_error[6] = 1;
                        error = '<button type = "button"  class="butnn btn-danger ">' + count_error() + '</button>';
                        route_error(route.data.state, route.data.name );
                      }
                      statusHTML += '</tr>';
                
                      document.querySelector('.routestatus_1 tbody.route_7').innerHTML = statusHTML;
                      document.querySelector('.routestatus_2 tbody.route_7').innerHTML = statusHTML;
                      document.querySelector('.routestatus_3 tbody.route_7').innerHTML = statusHTML;
                      document.querySelector('.routestatus_4 tbody.route_7').innerHTML = statusHTML;
                      if(error !== null && error !== undefined && (error !== ""))
          {
            document.querySelector('#STATUS ').innerHTML = error;
            document.querySelector('#STATUS_1 ').innerHTML = error;
            document.querySelector('#STATUS_2 ').innerHTML = error;
            document.querySelector('#STATUS_3 ').innerHTML = error;
          }else if(retrying !== null && retrying !== undefined && (retrying !== ""))
          {
          document.querySelector('#STATUS').innerHTML = retrying;
          document.querySelector('#STATUS_1').innerHTML = retrying;
          document.querySelector('#STATUS_2').innerHTML = retrying;
          document.querySelector('#STATUS_3').innerHTML = retrying;
          }else if(stopped !== null && stopped !== undefined && (stopped !== ""))
          {
          document.querySelector('#STATUS').innerHTML = stopped;
          document.querySelector('#STATUS_1').innerHTML = stopped;
          document.querySelector('#STATUS_2').innerHTML = stopped;
          document.querySelector('#STATUS_3').innerHTML = stopped;
          }else if(running !== null && running !== undefined && (running !== "")){
            document.querySelector('#STATUS ').innerHTML = running;
            document.querySelector('#STATUS_1 ').innerHTML = running;
            document.querySelector('#STATUS_2 ').innerHTML = running;
            document.querySelector('#STATUS_3 ').innerHTML = running;
          }else {
            var info = '';
            info = '<button type = "button"  class="butnn btn-danger " value = "NO ERROR">'  + '</button>';
          }
                      interval = setTimeout(callroute_1, 30000);
                  }
                }
                
                    xhttp_one.open('GET', comm, true);
                    xhttp_one.send();
                
                }
                callroute_7(); 


                function callroute_8(comm) {
                  var xhttp_one = new XMLHttpRequest();
                  
                  xhttp_one.onreadystatechange = function() {
                    if(xhttp_one.readyState === 4 ) {
                      var route = JSON.parse(xhttp_one.responseText);
                      console.log(route);
                      
                      var statusHTML = '';
                      var error = '';
                      var retrying = '';
                      var running = '';
                        statusHTML += '<tr>';
                        if(route.data.state === 'RUNNING') {
                          statusHTML += '<tr class = "success">';
                          statusHTML += '<td>'+ route.data.id + '</td>';
                          statusHTML += '<td>'+ route.data.name + '</td>';
                          statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                          statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                          statusHTML += '<td>' + route.data.processedCount + '</td>';
                          statusHTML += '<td>' + route.data.state + '</td>';
                          console.log(statusHTML);
                        } else if(route.data.state === 'RETRYING') {
                          statusHTML += '<tr class = "warning">';
                          statusHTML += '<td>'+ route.data.id + '</td>';
                          statusHTML += '<td>'+ route.data.name + '</td>';
                          statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                          statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                          statusHTML += '<td>' + route.data.processedCount + '</td>';
                          statusHTML += '<td>' + route.data.state + '</td>';
                          retrying = '<button type = "button"  class="butnn btn-warning ">' + route.data.state + '</button>';
                        } else if(route.data.state === 'STOPPED') {
                          statusHTML += '<tr class = "danger">';
                          statusHTML += '<td>'+route.data.id + '</td>';
                          statusHTML += '<td>'+ route.data.name + '</td>';
                          statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                          statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                          statusHTML += '<td>' + route.data.processedCount + '</td>';
                          statusHTML += '<td>' + route.data.state + '</td>';
                
                          num_stop[7] = 1;
                          stopped = '<button type = "button"  class="butnn btn-danger ">' + count_stopped() + '</button>';
                          route_stopped(route.data.state, route.data.name);
                
                        }else if(route.data.state === 'ERROR'){
                          statusHTML += '<tr class = "danger">';
                          statusHTML += '<td>'+ route.data.id + '</td>';
                          statusHTML += '<td>'+ route.data.name + '</td>';
                          statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                          statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                          statusHTML += '<td>' + route.data.processedCount + '</td>';
                          statusHTML += '<td>' + route.data.state + '</td>';
                
                          num_error[7] = 1;
                          error = '<button type = "button"  class="butnn btn-danger ">' + count_error() + '</button>';
                          route_error(route.data.state, route.data.name );
                        }
                        statusHTML += '</tr>';
                  
                        document.querySelector('.routestatus_1 tbody.route_8').innerHTML = statusHTML;
                        document.querySelector('.routestatus_2 tbody.route_8').innerHTML = statusHTML;
                        document.querySelector('.routestatus_3 tbody.route_8').innerHTML = statusHTML;
                        document.querySelector('.routestatus_4 tbody.route_8').innerHTML = statusHTML;
                        if(error !== null && error !== undefined && (error !== ""))
          {
            document.querySelector('#STATUS ').innerHTML = error;
            document.querySelector('#STATUS_1 ').innerHTML = error;
            document.querySelector('#STATUS_2 ').innerHTML = error;
            document.querySelector('#STATUS_3 ').innerHTML = error;
          }else if(retrying !== null && retrying !== undefined && (retrying !== ""))
          {
          document.querySelector('#STATUS').innerHTML = retrying;
          document.querySelector('#STATUS_1').innerHTML = retrying;
          document.querySelector('#STATUS_2').innerHTML = retrying;
          document.querySelector('#STATUS_3').innerHTML = retrying;
          }else if(stopped !== null && stopped !== undefined && (stopped !== ""))
          {
          document.querySelector('#STATUS').innerHTML = stopped;
          document.querySelector('#STATUS_1').innerHTML = stopped;
          document.querySelector('#STATUS_2').innerHTML = stopped;
          document.querySelector('#STATUS_3').innerHTML = stopped;
          }else if(running !== null && running !== undefined && (running !== "")){
            document.querySelector('#STATUS ').innerHTML = running;
            document.querySelector('#STATUS_1 ').innerHTML = running;
            document.querySelector('#STATUS_2 ').innerHTML = running;
            document.querySelector('#STATUS_3 ').innerHTML = running;
          }else {
            var info = '';
            info = '<button type = "button"  class="butnn btn-danger " value = "NO ERROR">'  + '</button>';
          }
                        interval = setTimeout(callroute_1, 30000);
                    }
                  }
                  
                      xhttp_one.open('GET', comm, true);
                      xhttp_one.send();
                  
                  }
                  callroute_8(); 


                  function callroute_9(comm) {
                    var xhttp_one = new XMLHttpRequest();
                    
                    xhttp_one.onreadystatechange = function() {
                      if(xhttp_one.readyState === 4 ) {
                        var route = JSON.parse(xhttp_one.responseText);
                        console.log(route);
                        
                        var statusHTML = '';
                        var error = '';
                        var retrying = '';
                        var running = '';
                          statusHTML += '<tr>';
                          if(route.data.state === 'RUNNING') {
                            statusHTML += '<tr class = "success">';
                            statusHTML += '<td>'+ route.data.id + '</td>';
                            statusHTML += '<td>'+ route.data.name + '</td>';
                            statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                            statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                            statusHTML += '<td>' + route.data.processedCount + '</td>';
                            statusHTML += '<td>' + route.data.state + '</td>';
                            console.log(statusHTML);
                          } else if(route.data.state === 'RETRYING') {
                            statusHTML += '<tr class = "warning">';
                            statusHTML += '<td>'+ route.data.id + '</td>';
                            statusHTML += '<td>'+ route.data.name + '</td>';
                            statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                            statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                            statusHTML += '<td>' + route.data.processedCount + '</td>';
                            statusHTML += '<td>' + route.data.state + '</td>';
                            retrying = '<button type = "button"  class="butnn btn-warning ">' + route.data.state + '</button>';
                          } else if(route.data.state === 'STOPPED') {
                            statusHTML += '<tr class = "danger">';
                            statusHTML += '<td>'+ route.data.id + '</td>';
                            statusHTML += '<td>'+ route.data.name + '</td>';
                            statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                            statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                            statusHTML += '<td>' + route.data.processedCount + '</td>';
                            statusHTML += '<td>' + route.data.state + '</td>';
                            num_stop[8] = 1;
                            stopped = '<button type = "button"  class="butnn btn-danger ">' + count_stopped() + '</button>';
                            route_stopped(route.data.state, route.data.name);
                  
                          }else if(route.data.state === 'ERROR'){
                            statusHTML += '<tr class = "danger">';
                            statusHTML += '<td>'+ route.data.id + '</td>';
                            statusHTML += '<td>'+ route.data.name + '</td>';
                            statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                            statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                            statusHTML += '<td>' + route.data.processedCount + '</td>';
                            statusHTML += '<td>' + route.data.state + '</td>';
                  
                            num_error[8] = 1;
                            error = '<button type = "button"  class="butnn btn-danger ">' + count_error() + '</button>';
                            route_error(route.data.state, route.data.name );
                          }
                          statusHTML += '</tr>';
                    
                          document.querySelector('.routestatus_1 tbody.route_9').innerHTML = statusHTML;
                          document.querySelector('.routestatus_2 tbody.route_9').innerHTML = statusHTML;
                          document.querySelector('.routestatus_3 tbody.route_9').innerHTML = statusHTML;
                          document.querySelector('.routestatus_4 tbody.route_9').innerHTML = statusHTML;
                          if(error !== null && error !== undefined && (error !== ""))
          {
            document.querySelector('#STATUS ').innerHTML = error;
            document.querySelector('#STATUS_1 ').innerHTML = error;
            document.querySelector('#STATUS_2 ').innerHTML = error;
            document.querySelector('#STATUS_3 ').innerHTML = error;
          }else if(retrying !== null && retrying !== undefined && (retrying !== ""))
          {
          document.querySelector('#STATUS').innerHTML = retrying;
          document.querySelector('#STATUS_1').innerHTML = retrying;
          document.querySelector('#STATUS_2').innerHTML = retrying;
          document.querySelector('#STATUS_3').innerHTML = retrying;
          }else if(stopped !== null && stopped !== undefined && (stopped !== ""))
          {
          document.querySelector('#STATUS').innerHTML = stopped;
          document.querySelector('#STATUS_1').innerHTML = stopped;
          document.querySelector('#STATUS_2').innerHTML = stopped;
          document.querySelector('#STATUS_3').innerHTML = stopped;
          }else if(running !== null && running !== undefined && (running !== "")){
            document.querySelector('#STATUS ').innerHTML = running;
            document.querySelector('#STATUS_1 ').innerHTML = running;
            document.querySelector('#STATUS_2 ').innerHTML = running;
            document.querySelector('#STATUS_3 ').innerHTML = running;
          }else {
            var info = '';
            info = '<button type = "button"  class="butnn btn-danger " value = "NO ERROR">'  + '</button>';
          }
                          interval = setTimeout(callroute_1, 30000);
                      }
                    }
                    
                        xhttp_one.open('GET', comm, true);
                        xhttp_one.send();
                    
                    }
                    callroute_9(); 


                    function callroute_10(comm) {
                      var xhttp_one = new XMLHttpRequest();
                      
                      xhttp_one.onreadystatechange = function() {
                        if(xhttp_one.readyState === 4 ) {
                          var route = JSON.parse(xhttp_one.responseText);
                          console.log(route);
                          
                          var statusHTML = '';
                          var error = '';
                          var retrying = '';
                          var running = '';
                            statusHTML += '<tr>';
                            if(route.data.state === 'RUNNING') {
                              statusHTML += '<tr class = "success">';
                              statusHTML += '<td>'+ route.data.id + '</td>';
                              statusHTML += '<td>'+ route.data.name + '</td>';
                              statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                              statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                              statusHTML += '<td>' + route.data.processedCount + '</td>';
                              statusHTML += '<td>' + route.data.state + '</td>';
                              console.log(statusHTML);
                            } else if(route.data.state === 'RETRYING') {
                              statusHTML += '<tr class = "warning">';
                              statusHTML += '<td>'+ route.data.id + '</td>';
                              statusHTML += '<td>'+ route.data.name + '</td>';
                              statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                              statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                              statusHTML += '<td>' + route.data.processedCount + '</td>';
                              statusHTML += '<td>' + route.data.state + '</td>';
                              retrying = '<button type = "button"  class="butnn btn-warning ">' + route.data.state + '</button>';
                            } else if(route.data.state === 'STOPPED') {
                              statusHTML += '<tr class = "danger">';
                              statusHTML += '<td>'+ route.data.id + '</td>';
                              statusHTML += '<td>'+ route.data.name + '</td>';
                              statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                              statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                              statusHTML += '<td>' + route.data.processedCount + '</td>';
                              statusHTML += '<td>' + route.data.state + '</td>';
                    
                              num_stop[9] = 1;
                              stopped = '<button type = "button"  class="butnn btn-danger ">' + count_stopped() + '</button>';
                              route_stopped(route.data.state, route.data.name);
                    
                            }else if(route.data.state === 'ERROR'){
                              statusHTML += '<tr class = "danger">';
                              statusHTML += '<td>'+ route.data.id + '</td>';
                              statusHTML += '<td>'+ route.data.name + '</td>';
                              statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                              statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                              statusHTML += '<td>' + route.data.processedCount + '</td>';
                              statusHTML += '<td>' + route.data.state + '</td>';
                    
                              num_error[9] = 1;
                              error = '<button type = "button"  class="butnn btn-danger ">' + count_error() + '</button>';
                              route_error(route.data.state, route.data.name );
                            }
                            statusHTML += '</tr>';
                      
                            document.querySelector('.routestatus_1 tbody.route_10').innerHTML = statusHTML;
                            document.querySelector('.routestatus_2 tbody.route_10').innerHTML = statusHTML;
                            document.querySelector('.routestatus_3 tbody.route_10').innerHTML = statusHTML;
                            document.querySelector('.routestatus_4 tbody.route_10').innerHTML = statusHTML;
                            if(error !== null && error !== undefined && (error !== ""))
          {
            document.querySelector('#STATUS ').innerHTML = error;
            document.querySelector('#STATUS_1 ').innerHTML = error;
            document.querySelector('#STATUS_2 ').innerHTML = error;
            document.querySelector('#STATUS_3 ').innerHTML = error;
          }else if(retrying !== null && retrying !== undefined && (retrying !== ""))
          {
          document.querySelector('#STATUS').innerHTML = retrying;
          document.querySelector('#STATUS_1').innerHTML = retrying;
          document.querySelector('#STATUS_2').innerHTML = retrying;
          document.querySelector('#STATUS_3').innerHTML = retrying;
          }else if(stopped !== null && stopped !== undefined && (stopped !== ""))
          {
          document.querySelector('#STATUS').innerHTML = stopped;
          document.querySelector('#STATUS_1').innerHTML = stopped;
          document.querySelector('#STATUS_2').innerHTML = stopped;
          document.querySelector('#STATUS_3').innerHTML = stopped;
          }else if(running !== null && running !== undefined && (running !== "")){
            document.querySelector('#STATUS ').innerHTML = running;
            document.querySelector('#STATUS_1 ').innerHTML = running;
            document.querySelector('#STATUS_2 ').innerHTML = running;
            document.querySelector('#STATUS_3 ').innerHTML = running;
          }else {
            var info = '';
            info = '<button type = "button"  class="butnn btn-danger " value = "NO ERROR">'  + '</button>';
          }
                            interval = setTimeout(callroute_1, 30000);
                        }
                      }
                      
                          xhttp_one.open('GET', comm, true);
                          xhttp_one.send();
                      
                      }
                      callroute_10(); 



                      function callroute_11(comm) {
                        var xhttp_one = new XMLHttpRequest();
                        
                        xhttp_one.onreadystatechange = function() {
                          if(xhttp_one.readyState === 4 ) {
                            var route = JSON.parse(xhttp_one.responseText);
                            console.log(route);
                            
                            var statusHTML = '';
                            var error = '';
                            var retrying = '';
                            var running = '';
                              statusHTML += '<tr>';
                              if(route.data.state === 'RUNNING') {
                                statusHTML += '<tr class = "success">';
                                statusHTML += '<td>'+ route.data.id + '</td>';
                                statusHTML += '<td>'+ route.data.name + '</td>';
                                statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                                statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                                statusHTML += '<td>' + route.data.processedCount + '</td>';
                                statusHTML += '<td>' + route.data.state + '</td>';
                                console.log(statusHTML);
                              } else if(route.data.state === 'RETRYING') {
                                statusHTML += '<tr class = "warning">';
                                statusHTML += '<td>'+ route.data.id + '</td>';
                                statusHTML += '<td>'+ route.data.name + '</td>';
                                statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                                statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                                statusHTML += '<td>' + route.data.processedCount + '</td>';
                                statusHTML += '<td>' + route.data.state + '</td>';
                                retrying = '<button type = "button"  class="butnn btn-warning ">' + route.data.state + '</button>';
                              } else if(route.data.state === 'STOPPED') {
                                statusHTML += '<tr class = "danger">';
                                statusHTML += '<td>'+ route.data.id + '</td>';
                                statusHTML += '<td>'+ route.data.name + '</td>';
                                statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                                statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                                statusHTML += '<td>' + route.data.processedCount + '</td>';
                                statusHTML += '<td>' + route.data.state + '</td>';
                      
                                num_stop[10] = 1;
                                stopped = '<button type = "button"  class="butnn btn-danger ">' + count_stopped() + '</button>';
                                route_stopped(route.data.state, route.data.name);
                      
                              }else if(route.data.state === 'ERROR'){
                                statusHTML += '<tr class = "danger">';
                                statusHTML += '<td>'+ route.data.id + '</td>';
                                statusHTML += '<td>'+ route.data.name + '</td>';
                                statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                                statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                                statusHTML += '<td>' + route.data.processedCount + '</td>';
                                statusHTML += '<td>' + route.data.state + '</td>';
                      
                                num_error[10] = 1;
                                error = '<button type = "button"  class="butnn btn-danger ">' + count_error() + '</button>';
                                route_error(route.data.state, route.data.name );
                              }
                              statusHTML += '</tr>';
                        
                              document.querySelector('.routestatus_1 tbody.route_11').innerHTML = statusHTML;
                              document.querySelector('.routestatus_2 tbody.route_11').innerHTML = statusHTML;
                              document.querySelector('.routestatus_3 tbody.route_11').innerHTML = statusHTML;
                              document.querySelector('.routestatus_4 tbody.route_11').innerHTML = statusHTML;
                              if(error !== null && error !== undefined && (error !== ""))
          {
            document.querySelector('#STATUS ').innerHTML = error;
            document.querySelector('#STATUS_1 ').innerHTML = error;
            document.querySelector('#STATUS_2 ').innerHTML = error;
            document.querySelector('#STATUS_3 ').innerHTML = error;
          }else if(retrying !== null && retrying !== undefined && (retrying !== ""))
          {
          document.querySelector('#STATUS').innerHTML = retrying;
          document.querySelector('#STATUS_1').innerHTML = retrying;
          document.querySelector('#STATUS_2').innerHTML = retrying;
          document.querySelector('#STATUS_3').innerHTML = retrying;
          }else if(stopped !== null && stopped !== undefined && (stopped !== ""))
          {
          document.querySelector('#STATUS').innerHTML = stopped;
          document.querySelector('#STATUS_1').innerHTML = stopped;
          document.querySelector('#STATUS_2').innerHTML = stopped;
          document.querySelector('#STATUS_3').innerHTML = stopped;
          }else if(running !== null && running !== undefined && (running !== "")){
            document.querySelector('#STATUS ').innerHTML = running;
            document.querySelector('#STATUS_1 ').innerHTML = running;
            document.querySelector('#STATUS_2 ').innerHTML = running;
            document.querySelector('#STATUS_3 ').innerHTML = running;
          }else {
            var info = '';
            info = '<button type = "button"  class="butnn btn-danger " value = "NO ERROR">'  + '</button>';
          }
                              interval = setTimeout(callroute_1, 30000);
                          }
                        }
                        
                            xhttp_one.open('GET', comm, true);
                            xhttp_one.send();
                        
                        }
                        callroute_11(); 

                        function callroute_12(comm) {
                          var xhttp_one = new XMLHttpRequest();
                          
                          xhttp_one.onreadystatechange = function() {
                            if(xhttp_one.readyState === 4 ) {
                              var route = JSON.parse(xhttp_one.responseText);
                              console.log(route);
                              
                              var statusHTML = '';
                              var error = '';
                              var retrying = '';
                              var running = '';
                                statusHTML += '<tr>';
                                if(route.data.state === 'RUNNING') {
                                  statusHTML += '<tr class = "success">';
                                  statusHTML += '<td>'+ route.data.id + '</td>';
                                  statusHTML += '<td>'+ route.data.name + '</td>';
                                  statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                                  statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                                  statusHTML += '<td>' + route.data.processedCount + '</td>';
                                  statusHTML += '<td>' + route.data.state + '</td>';
                                  console.log(statusHTML);
                                } else if(route.data.state === 'RETRYING') {
                                  statusHTML += '<tr class = "warning">';
                                  statusHTML += '<td>'+ route.data.id + '</td>';
                                  statusHTML += '<td>'+ route.data.name + '</td>';
                                  statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                                  statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                                  statusHTML += '<td>' + route.data.processedCount + '</td>';
                                  statusHTML += '<td>' + route.data.state + '</td>';
                                  retrying = '<button type = "button"  class="butnn btn-warning ">' + route.data.state + '</button>';
                                } else if(route.data.state === 'STOPPED') {
                                  statusHTML += '<tr class = "danger">';
                                  statusHTML += '<td>'+ route.data.id + '</td>';
                                  statusHTML += '<td>'+ route.data.name + '</td>';
                                  statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                                  statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                                  statusHTML += '<td>' + route.data.processedCount + '</td>';
                                  statusHTML += '<td>' + route.data.state + '</td>';
                        
                                  num_stop[11] = 1;
                                  stopped = '<button type = "button"  class="butnn btn-danger ">' + count_stopped() + '</button>';
                                  route_stopped(route.data.state, route.data.name);
                        
                                }else if(route.data.state === 'ERROR'){
                                  statusHTML += '<tr class = "danger">';
                                  statusHTML += '<td>'+route.data.id + '</td>';
                                  statusHTML += '<td>'+ route.data.name + '</td>';
                                  statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                                  statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                                  statusHTML += '<td>' + route.data.processedCount + '</td>';
                                  statusHTML += '<td>' + route.data.state + '</td>';
                        
                                  num_error[11] = 1;
                                  error = '<button type = "button"  class="butnn btn-danger ">' + count_error() + '</button>';
                                  route_error(route.data.state, route.data.name );
                                }
                                statusHTML += '</tr>';
                          
                                document.querySelector('.routestatus_1 tbody.route_12').innerHTML = statusHTML;
                                document.querySelector('.routestatus_2 tbody.route_12').innerHTML = statusHTML;
                                document.querySelector('.routestatus_3 tbody.route_12').innerHTML = statusHTML;
                                document.querySelector('.routestatus_4 tbody.route_12').innerHTML = statusHTML;
                                if(error !== null && error !== undefined && (error !== ""))
                                {
                                  document.querySelector('#STATUS ').innerHTML = error;
                                  document.querySelector('#STATUS_1 ').innerHTML = error;
                                  document.querySelector('#STATUS_2 ').innerHTML = error;
                                  document.querySelector('#STATUS_3 ').innerHTML = error;
                                }else if(retrying !== null && retrying !== undefined && (retrying !== ""))
                                {
                                document.querySelector('#STATUS').innerHTML = retrying;
                                document.querySelector('#STATUS_1').innerHTML = retrying;
                                document.querySelector('#STATUS_2').innerHTML = retrying;
                                document.querySelector('#STATUS_3').innerHTML = retrying;
                                }else if(stopped !== null && stopped !== undefined && (stopped !== ""))
                                {
                                document.querySelector('#STATUS').innerHTML = stopped;
                                document.querySelector('#STATUS_1').innerHTML = stopped;
                                document.querySelector('#STATUS_2').innerHTML = stopped;
                                document.querySelector('#STATUS_3').innerHTML = stopped;
                                }else if(running !== null && running !== undefined && (running !== "")){
                                  document.querySelector('#STATUS ').innerHTML = running;
                                  document.querySelector('#STATUS_1 ').innerHTML = running;
                                  document.querySelector('#STATUS_2 ').innerHTML = running;
                                  document.querySelector('#STATUS_3 ').innerHTML = running;
                                }else {
                                  var info = '';
                                  info = '<button type = "button"  class="butnn btn-danger " value = "NO ERROR">'  + '</button>';
                                }
                                interval = setTimeout(callroute_1, 30000);
                            }
                          }
                          
                              xhttp_one.open('GET', comm, true);
                              xhttp_one.send();
                          
                          }
                          callroute_12(); 

                          function callroute_13(comm) {
                            var xhttp_one = new XMLHttpRequest();
                            
                            xhttp_one.onreadystatechange = function() {
                              if(xhttp_one.readyState === 4 ) {
                                var route = JSON.parse(xhttp_one.responseText);
                                console.log(route);
                                var statusHTML = '';
                                var error = '';
                                var retrying = '';
                                var running = '';
                                  statusHTML += '<tr>';
                                  if(route.data.state === 'RUNNING') {
                                    statusHTML += '<tr class = "success">';
                                    statusHTML += '<td>'+ route.data.id + '</td>';
                                    statusHTML += '<td>'+ route.data.name + '</td>';
                                    statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                                    statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                                    statusHTML += '<td>' + route.data.processedCount + '</td>';
                                    statusHTML += '<td>' + route.data.state + '</td>';
                                  } else if(route.data.state === 'RETRYING') {
                                    statusHTML += '<tr class = "warning">';
                                    statusHTML += '<td>'+ route.data.id + '</td>';
                                    statusHTML += '<td>'+ route.data.name + '</td>';
                                    statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                                    statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                                    statusHTML += '<td>' + route.data.processedCount + '</td>';
                                    statusHTML += '<td>' + route.data.state + '</td>'; 
                                    retrying = '<button type = "button"  class="butnn btn-warning ">' + route.data.state + '</button>';
                                  } else if(route.data.state === 'STOPPED') {
                                    statusHTML += '<tr class = "danger">';
                                    statusHTML += '<td>'+ route.data.id + '</td>';
                                    statusHTML += '<td>'+ route.data.name + '</td>';
                                    statusHTML += '<td>'+ route.data.outQueueSize + '</td>';
                                    statusHTML += '<td>' + route.data.receivedCount + '</td>';
                                    statusHTML += '<td>' + route.data.receivedCount + '</td>';
                                    statusHTML += '<td>' + route.data.state + '</td>';
                          
                                    num_stop[12] = 1;
                                    stopped = '<button type = "button"  class="butnn btn-danger ">' + count_stopped() + '</button>';
                                    route_stopped(route.data.state, route.data.name);
                          
                                  }else if(route.data.state === 'ERROR'){
                                    statusHTML += '<tr class = "danger">';
                                    statusHTML += '<td>'+ route.data.id + '</td>';
                                    statusHTML += '<td>'+ route.data.name + '</td>';
                                    statusHTML += '<td>'+ route.data.outQueueSize + '</td>';
                                    statusHTML += '<td>' + route.data.receivedCount + '</td>';
                                    statusHTML += '<td>' + route.data.receivedCount + '</td>';
                                    statusHTML += '<td>' + route.data.state + '</td>';
                          
                                    num_error[12] = 1;
                                    error = '<button type = "button"  class="butnn btn-danger ">' + count_error() + '</button>';
                                    route_error(route.data.state, route.data.name );
                                  }
                                  statusHTML += '</tr>';
                            
                                  document.querySelector('.routestatus_1 tbody.route_13').innerHTML = statusHTML;
                                  document.querySelector('.routestatus_2 tbody.route_13').innerHTML = statusHTML;
                                  document.querySelector('.routestatus_3 tbody.route_13').innerHTML = statusHTML;
                                  document.querySelector('.routestatus_4 tbody.route_13').innerHTML = statusHTML;
                                  if(error !== null && error !== undefined && (error !== ""))
                                  {
                                    document.querySelector('#STATUS ').innerHTML = error;
                                    document.querySelector('#STATUS_1 ').innerHTML = error;
                                    document.querySelector('#STATUS_2 ').innerHTML = error;
                                    document.querySelector('#STATUS_3 ').innerHTML = error;
                                  }else if(retrying !== null && retrying !== undefined && (retrying !== ""))
                                  {
                                  document.querySelector('#STATUS').innerHTML = retrying;
                                  document.querySelector('#STATUS_1').innerHTML = retrying;
                                  document.querySelector('#STATUS_2').innerHTML = retrying;
                                  document.querySelector('#STATUS_3').innerHTML = retrying;
                                  }else if(stopped !== null && stopped !== undefined && (stopped !== ""))
                                  {
                                  document.querySelector('#STATUS').innerHTML = stopped;
                                  document.querySelector('#STATUS_1').innerHTML = stopped;
                                  document.querySelector('#STATUS_2').innerHTML = stopped;
                                  document.querySelector('#STATUS_3').innerHTML = stopped;
                                  }else if(running !== null && running !== undefined && (running !== "")){
                                    document.querySelector('#STATUS ').innerHTML = running;
                                    document.querySelector('#STATUS_1 ').innerHTML = running;
                                    document.querySelector('#STATUS_2 ').innerHTML = running;
                                    document.querySelector('#STATUS_3 ').innerHTML = running;
                                  }else {
                                    var info = '';
                                    info = '<button type = "button"  class="butnn btn-danger " value = "NO ERROR">'  + '</button>';
                                  }
                                  interval = setTimeout(callroute_1, 30000);
                              }
                            }
                            
                                xhttp_one.open('GET', comm, true);
                                xhttp_one.send();
                            }
                            callroute_13(); 

                            function callroute_14(comm) {
                              var xhttp_one = new XMLHttpRequest();
                              
                              xhttp_one.onreadystatechange = function() {
                                if(xhttp_one.readyState === 4 ) {
                                  var route = JSON.parse(xhttp_one.responseText);
                                  console.log(route);
                                  
                                  var statusHTML = '';
                                  var running = '';
                                  var error = '';
                                  var retrying = '';
                                  statusHTML += '<tr>';
                                  if(route.data.state === 'RUNNING') {
                                    statusHTML += '<tr class = "success">';
                                    statusHTML += '<td>'+ route.data.id + '</td>';
                                    statusHTML += '<td>'+ route.data.name + '</td>';
                                    statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                                    statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                                    statusHTML += '<td>' + route.data.processedCount + '</td>';
                                    statusHTML += '<td>' + route.data.state + '</td>';
                                    console.log(statusHTML);
                                  } else if(route.data.state === 'RETRYING') {
                                    statusHTML += '<tr class = "warning">';
                                    statusHTML += '<td>'+ route.data.id + '</td>';
                                    statusHTML += '<td>'+ route.data.name + '</td>';
                                    statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                                    statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                                    statusHTML += '<td>' + route.data.processedCount + '</td>';
                                    statusHTML += '<td>' + route.data.state + '</td>';
                                    retrying = '<button type = "button"  class="butnn btn-warning ">' + route.data.state + '</button>';
                                
                                  } else if(route.data.state === 'STOPPED') {
                                    statusHTML += '<tr class = "danger">';
                                    statusHTML += '<td>'+ route.data.id + '</td>';
                                    statusHTML += '<td>'+ route.data.name + '</td>';
                                    statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                                    statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                                    statusHTML += '<td>' + route.data.processedCount + '</td>';
                                    statusHTML += '<td>' + route.data.state + '</td>';
                            
                                    num_stop[13] = 1;
                                    stopped = '<button type = "button"  class="butnn btn-danger ">' + count_stopped() + '</button>';
                                    route_stopped(route.data.state, route.data.name);
                            
                                  }else if(route.data.state === 'ERROR'){
                                    statusHTML += '<tr class = "danger">';
                                    statusHTML += '<td>'+ route.data.id + '</td>';
                                    statusHTML += '<td>'+ route.data.name + '</td>';
                                    statusHTML += '<td>'+ route.data.waitingQueueSize + '</td>';
                                    statusHTML += '<td>' + route.data.processingQueueSize + '</td>';
                                    statusHTML += '<td>' + route.data.processedCount + '</td>';
                                    statusHTML += '<td>' + route.data.state + '</td>';
                            
                                    num_error[13] = 1;
                                    error = '<button type = "button"  class="butnn btn-danger ">' + count_error() + '</button>';
                                    route_error(route.data.state, route.data.name );
                                  }
                                  
                                  statusHTML += '</tr>';
                              
                                    document.querySelector('.routestatus_1 tbody.route_1').innerHTML = statusHTML;
                                    document.querySelector('.routestatus_2 tbody.route_1').innerHTML = statusHTML;
                                    document.querySelector('.routestatus_3 tbody.route_1').innerHTML = statusHTML;
                                    document.querySelector('.routestatus_4 tbody.route_1').innerHTML = statusHTML;
                                    if(error !== null && error !== undefined && (error !== ""))
                                      {
                                        document.querySelector('#STATUS ').innerHTML = error;
                                        document.querySelector('#STATUS_1 ').innerHTML = error;
                                        document.querySelector('#STATUS_2 ').innerHTML = error;
                                        document.querySelector('#STATUS_3 ').innerHTML = error;
                                      }else if(retrying !== null && retrying !== undefined && (retrying !== ""))
                                      {
                                      document.querySelector('#STATUS').innerHTML = retrying;
                                      document.querySelector('#STATUS_1').innerHTML = retrying;
                                      document.querySelector('#STATUS_2').innerHTML = retrying;
                                      document.querySelector('#STATUS_3').innerHTML = retrying;
                                      }else if(stopped !== null && stopped !== undefined && (stopped !== ""))
                                      {
                                      document.querySelector('#STATUS').innerHTML = stopped;
                                      document.querySelector('#STATUS_1').innerHTML = stopped;
                                      document.querySelector('#STATUS_2').innerHTML = stopped;
                                      document.querySelector('#STATUS_3').innerHTML = stopped;
                                      }else if(running !== null && running !== undefined && (running !== "")){
                                        document.querySelector('#STATUS ').innerHTML = running;
                                        document.querySelector('#STATUS_1 ').innerHTML = running;
                                        document.querySelector('#STATUS_2 ').innerHTML = running;
                                        document.querySelector('#STATUS_3 ').innerHTML = running;
                                      }else {
                                        var info = '';
                                        info = '<button type = "button"  class="butnn btn-danger " value = "NO ERROR">'  + '</button>';
                                      }
                            
                                    interval = setTimeout(callroute_1, 30000);
                                }
                              }
                              
                                  xhttp_one.open('GET', comm, true);
                                  xhttp_one.send();
                              
                              }
                              callroute_14(); 

                            function route_error(co , name) {
                              var text = "Monitoring Status" + "\n" + "\n" + "Name:" + name + "\n" + "TimeStamp:" + getFormattedTime() + "\n" +"Type:" + "Routes" + "\n" + "Status:" + co;
                              var filename = getFormattedTime();
                              var blob = new Blob([text], {type: "text/plain;charset=utf-8"});
                              saveAs(blob, filename+".txt");
                            };
                          
                          
                            function route_stopped(co, name) {
                              var text = "Monitoring Status" + "\n" + "\n" + "Name:" + name + "\n" + "TimeStamp:" + getFormattedTime() + "\n" +"Type:" + "Routes" + "\n" + "Status:" + co;
                              var filename = getFormattedTime();
                              var blob = new Blob([text], {type: "text/plain;charset=utf-8"});
                              /*saveAs(blob, filename+".txt");*/
                            };
                          
                          
                            function getFormattedTime() {
                              var today = new Date();
                              var y = today.getFullYear();
                              // JavaScript months are 0-based.
                              var m = today.getMonth() + 1;
                              var d = today.getDate();
                              var h = today.getHours();
                              var mi = today.getMinutes();
                              var s = today.getSeconds();
                              return (y + "/" + m + "/" + d + "  " + h + ":" + mi + ":" + s);
                          }

                          function count_stopped(){
                            return(num_stop[13]+num_stop[0] + num_stop[1]+num_stop[2] + num_stop[3]+num_stop[4] + num_stop[5]+num_stop[6] + num_stop[7]+num_stop[8] + num_stop[9]+num_stop[10] + num_stop[11]+num_stop[12]);
                          }
                          
                          function count_error(){
                            return(num_error[13]+num_error[0] + num_error[1]+num_error[2] + num_error[3]+num_error[4] + num_error[5]+num_error[6] + num_error[7]+num_error[8] + num_error[9]+num_error[10] + num_error[11]+num_error[12]);
                          }          