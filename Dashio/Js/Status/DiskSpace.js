
function callcommpoints_Disk(commp) {
    var xhttp_one = new XMLHttpRequest();
    
    xhttp_one.onreadystatechange = function() {
      if(xhttp_one.readyState === 4 ) {
        var commpoints = JSON.parse(xhttp_one.responseText);
        console.log(commpoints);
        var num = parseFloat(((commpoints.data.totalDataSpace - commpoints.data.availableDataSpace)/commpoints.data.totalDataSpace) * 100);
        var statusHTML = '';
          statusHTML += '<h3>';
          statusHTML += num.toFixed(2) + '%';
          statusHTML += '</h3>';
          var doughnutData = [{
            value: ((commpoints.data.totalDataSpace - commpoints.data.availableDataSpace)/commpoints.data.totalDataSpace) * 100,
            color: "#808000"
          },
          {
            value: (100 - ((commpoints.data.totalDataSpace - commpoints.data.availableDataSpace)/commpoints.data.totalDataSpace) * 100),
            color: "#fdfdfd"
          }
        ];
          var myDoughnut = new Chart(document.getElementById("serverstatus02").getContext("2d")).Doughnut(doughnutData);
          document.querySelector('#Disk_Status h3').innerHTML = statusHTML;
          
          interval = setTimeout(callcommpoints_Disk, 30000);
      }
    }
    
        xhttp_one.open('GET', "https://localhost:8444/api/statistics/diskspace", true);
        xhttp_one.send();
    
    }
    callcommpoints_Disk(); 


             
              